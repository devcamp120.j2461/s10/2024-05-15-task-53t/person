import test.Person;

public class App {
    public static void main(String[] args) throws Exception {
        System.out.println("Hello, World!");
        Person personA = new Person();
        personA.setFirstName("Tony");
        personA.setLastName("Hoang");
        personA.setEmail("tony@gmail.com");
        personA.setGender("male");
        personA.setContact("9812312323");

        System.out.println(personA);

        Person personB = new Person("Tony", "Le", "male", "08912312321", "a@gmail.com");
        System.out.println(personB);
    }
}
